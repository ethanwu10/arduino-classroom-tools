#include "Arduino.h"
#include "ArduinoMock.h"

#include <array>
#include <exception>
#include <sstream>

constexpr size_t numPins = 14; // 0-13

using namespace std;
using namespace ArduMock;

namespace
{
    array<uint8_t, numPins> modes;
    array<OutputT, numPins> outputModes;
    array<uint8_t, numPins> analogValues;
    array<bool, numPins> digitalValues;
}

void pinMode(uint8_t pin, uint8_t mode)
{
	if (pin > numPins)
	{
		ostringstream writer;
		writer << "Invalid pin number: " << (int)pin;
		throw invalid_argument(writer.str());
	}
    modes[pin] = mode;
}

void digitalWrite(uint8_t pin, uint8_t val)
{
	if (pin > numPins)
	{
		ostringstream writer;
		writer << "Invalid pin number: " << (int)pin;
		throw invalid_argument(writer.str());
	}
    outputModes[pin] = OutputT::Digital;
    digitalValues[pin] = val;
}

void analogWrite(uint8_t pin, uint8_t val)
{
	if (pin > numPins)
	{
		ostringstream writer;
		writer << "Invalid pin number: " << (int)pin;
		throw invalid_argument(writer.str());
	}
    pinMode(pin, OUTPUT);
    if (val == 0)
    {
        digitalWrite(pin, false);
        return;
    }
    else if (val == 255)
    {
        digitalWrite(pin, true);
        return;
    }

    outputModes[pin] = OutputT::Analog;
    analogValues[pin] = val;
}

namespace ArduMock
{
    void init()
    {}

    uint8_t getPinMode(uint8_t pin)
    {
        return modes[pin];
    }

    OutputT getPinOutputMode(uint8_t pin)
    {
        return outputModes[pin];
    }

    bool getPinDigitalValue(uint8_t pin)
    {
        return digitalValues[pin];
    }

    uint8_t getPinAnalogValue(uint8_t pin)
    {
        return analogValues[pin];
    }

    int MockStream::available()
    {
        return read_buffer.size();
    }

    int MockStream::read()
    {
        if (read_buffer.empty()) return -1;
        int v = read_buffer.front(); read_buffer.pop();
        return v;
    }

    int MockStream::peek()
    {
        if (read_buffer.empty()) return -1;
        return read_buffer.front();
    }

    void MockStream::write(uint8_t byte)
    {
        write_buffer.push_back(byte);
    }

    void MockStream::put_read(int val)
    {
        read_buffer.push(val);
    }

    const std::vector<uint8_t>& MockStream::get_write_buffer()
    {
        return write_buffer;
    }
}
