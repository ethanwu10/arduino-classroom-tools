#pragma once

#include <cstdint>
#include <vector>
#include <queue>

#include "Arduino.h"

namespace ArduMock
{
    void init();

    enum class OutputT
    { Digital, Analog };

    uint8_t getPinMode(uint8_t pin);

    OutputT getPinOutputMode(uint8_t pin);

    bool getPinDigitalValue(uint8_t pin);

    uint8_t getPinAnalogValue(uint8_t pin);

    class MockStream : public Stream
    {
    private:
        std::queue<int> read_buffer;
        std::vector<uint8_t> write_buffer;

    public:
        virtual int available();
        virtual int read();
        virtual int peek();

        virtual void write(uint8_t byte);

        // Internal-access functions

        void put_read(int val);
        const std::vector<uint8_t>& get_write_buffer();
    };
}
