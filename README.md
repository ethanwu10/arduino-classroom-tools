Arduino classroom tools
=======================

A set of helper files to facilitate writing assignments for Arduino on
[repl.it](//repl.it)

It uses [catch.hpp][catch] to write unit test cases and a set of mocking stubs
for the Ardunio libraries.

Currently supported functions:
- digital pins
- PWM pins

[catch]: //github.com/catchorg/Catch2
