#pragma once

#include <cstdint>
#include <cstddef>

constexpr uint8_t INPUT = 0;
constexpr uint8_t OUTPUT = 1;

constexpr bool HIGH = true;
constexpr bool LOW = false;

void pinMode(uint8_t pin, uint8_t mode);

void digitalWrite(uint8_t pin, uint8_t val);

void analogWrite(uint8_t pin, uint8_t val);

class Stream {
public:
    virtual int available() = 0;
    virtual int read() = 0;
    virtual int peek() = 0;

    virtual void write(uint8_t byte) = 0;
    virtual void write(const uint8_t* buf, size_t size) {
        for (size_t i = 0; i < size; ++i) {
            write(buf[i]);
        }
    }
};
